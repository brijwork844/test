from rest_framework import serializers
from .models import Jobs
from django_enum_choices.serializers import EnumChoiceField
from utils.helper import ScheduleStates
import time

class AllJobsSerializer(serializers.ModelSerializer):
    creator_name = serializers.CharField(source='created_by.username')
    creator_id = serializers.CharField(source='created_by.id')
    datasource_id = serializers.CharField(source='datasource.id')
    datasource_name = serializers.CharField(source='datasource.name')
    server_path = serializers.CharField(source='datasource.server_dir_path')
    schedule_type = EnumChoiceField(ScheduleStates)

    schedule_duration = serializers.SerializerMethodField('get_converted_date')

    def get_converted_date(self, obj):
        t = time.strptime(obj.schedule_duration, "%H:%M")
        sch = time.strftime( "%I:%M %p", t)
        return sch
        
    class Meta:
        model = Jobs
        fields = ('id','schedule_value', 'schedule_type','schedule_duration','max_historical_indices','is_repeat_scan', 'creator_id', 'creator_name',
                  'datasource_id','datasource_name', 'server_path', 'is_running','is_active','error_msg','created_at', 'modified_at')